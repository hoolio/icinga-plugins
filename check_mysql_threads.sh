#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# parses /usr/lib/nagios/plugins/check_mysql and grabs out the threadcount
#   twsadmin@arkaroola:/usr/local/scripts$ /usr/lib/nagios/plugins/check_mysql
#   Uptime: 377361  Threads: 1  Questions: 6535832  Slow queries: 0  Opens: 661  Flush tables: 1  Open tables: 383  Queries per second avg: 17.319
#
# errors are passed to nagios based on the thresholds passed as arguments;
#   usage ./check_mysql_threads.sh warning_threshold critical_threshold
#
# TO DO:
# 

use 5.010; 
use strict;
use warnings;
use Scalar::Util 'looks_like_number';

## PREAMBLE AND SETUP ##
#
# if you have permissions errors, try mysql> GRANT USAGE ON *.* TO nagios@'localhost';
my $mysql_username = "nagios";
my $mysql_password = "";

my $mysql_check_file = "/usr/lib/nagios/plugins/check_mysql";

# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_mysql_threads.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

# make sure this machine has check_mysql installed!
if (not -e $mysql_check_file) {	 print "ERROR: $mysql_check_file not found.  Install nagios-plugins-standard?\n"; exit 2; }

## DO THE CHECK AND PARSE THE OUTPUT
#
# Run the packaged generic check_mysql script
#  then split the output of that into useful chunks. 
my $results=`$mysql_check_file --username=$mysql_username --password=$mysql_password`; chomp($results);
my @results_split = split (/\s+/,$results);  
my $number_of_threads = $results_split[3]; chomp($number_of_threads);  

# we need to check if the output of the check_mysql command yeilded useful output, or errored.
#  we expect $number_of_threads to be an integer, so lets just check that's the case, else exit out.
if (not looks_like_number $number_of_threads) { print "ERROR: $results\n";  exit 2; }

# then drive the logic, returning the specific error codes nagios needs.
# note the performance data after the | delimiter in the print statements.  this is used for graphing, see below.
given($number_of_threads) {
	when ($number_of_threads <= $warning_threshold) {
		print "OK: Threadcount is $number_of_threads | threads=$number_of_threads;;$warning_threshold;$critical_threshold;0;"; 
		exit 0;
		}
	when ($number_of_threads > $warning_threshold and $number_of_threads < $critical_threshold) {
		print "WARNING: Threadcount is $number_of_threads | threads=$number_of_threads;;$warning_threshold;$critical_threshold;0;"; 
		exit 1; 
		}
	default {
		print "CRITICAL: Threadcount is $number_of_threads | threads=$number_of_threads;;$warning_threshold;$critical_threshold;0;";
		exit 2;
		}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value

