#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# parses the output of mythtv-status and returns how many days guide data we have
#
#hoolio@ferntree:~/scripts/icinga-plugins$ /usr/bin/lynx -dump localhost:6544/Status/GetStatusHTML | grep "There's guide data"
#   There's guide data until 2014-03-05 06:00 (8 days).

use 5.010; 
use strict;
use warnings;

## PREAMBLE AND SETUP ##
#
my $maximum_concievable_epg_data_days=9;

# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_shepherd_epg.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

## DO THE CHECK AND PARSE THE OUTPUT
#
# grab the stats from the modem as a large string
my $return=`/usr/bin/lynx -dump localhost:6544/Status/GetStatusHTML | grep "There's guide data"`; chomp($return);
my @return_split = split (/\s+/,$return);

my $remaining_days_epg_data=$return_split[7]; 
$remaining_days_epg_data =~ s/\W//g;

given($remaining_days_epg_data) {
	when ($remaining_days_epg_data >= $warning_threshold) {
		print "OK: $remaining_days_epg_data days of EPG data | down=$remaining_days_epg_data;$warning_threshold;$critical_threshold;0;$maximum_concievable_epg_data_days";
		exit 0;
		}
	when ($remaining_days_epg_data < $warning_threshold and $remaining_days_epg_data >= $critical_threshold) {
		print "WARNING: $remaining_days_epg_data days of EPG data | down=$remaining_days_epg_data;$warning_threshold;$critical_threshold;0;$maximum_concievable_epg_data_days";
		exit 1; 
		}
	default {
		print "CRITICAL: $remaining_days_epg_data days of EPG data | down=$remaining_days_epg_data;$warning_threshold;$critical_threshold;0;$maximum_concievable_epg_data_days";
		exit 2;
		}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value

