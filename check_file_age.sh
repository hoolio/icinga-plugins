#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# this script checks a file on disk and chucks an error if the file is older than X days.
#  the file age and the allowable age are passed in as arguments.
#
#   usage ./check_mysql_backups.sh file_to_check allowable_age_in_days
#
# the script will return;
#	OK if the file age is less than the allowable age
#	WARNING if the file age is less than a day over the allowable age.
#	CRITICAL if the file age is more than one day over the allowable age.
#
# TO DO:
#  drink beer.

# NOTE: because this check must run on my old servers, I'm using switch over given/when.
#use 5.010; 
use Switch;
use strict;
use warnings;
use Scalar::Util 'looks_like_number';

## PREAMBLE AND SETUP ##
#
# Parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_mysql_backups.sh file_to_check allowable_age_in_days\n"; exit 2;}
my $file_to_check = "$ARGV[0]\n"; chomp($file_to_check); 
my $allowable_age_in_days = "$ARGV[1]\n"; chomp($allowable_age_in_days);

# Do some error checking;
if (not -e $file_to_check) {  print "ERROR: $file_to_check does not exist!\n";  exit 2; }
if (not looks_like_number $allowable_age_in_days) { print "ERROR: $allowable_age_in_days is not a number\n";  exit 2; }

## DO THE CHECK AND PARSE THE OUTPUT
#
# Determine file age and round it to two decimal places
my $file_age_in_days = -M $file_to_check; $file_age_in_days = sprintf("%.2f", $file_age_in_days);

# Exit as appropriate;
switch( $file_age_in_days ){
	case {$file_age_in_days < $allowable_age_in_days}{
		print "OK: $file_to_check is $file_age_in_days days old\n"; 
		exit 0;
	}
	case {$file_age_in_days > $allowable_age_in_days and $file_age_in_days < $allowable_age_in_days + 1}{
		print "WARNING: $file_to_check is $file_age_in_days days old\n"; 
		exit 1;
	}
	case {$file_age_in_days > $allowable_age_in_days + 1}{
		print "CRITICAL: $file_to_check is $file_age_in_days days old\n"; 
		exit 2;
	}
}
