#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# to be used in tandem with a cronjob such as the following;
#  * * * * * /usr/bin/iostat -x -d /dev/sdb -t 58 2 | grep -n sdb | grep 9:sdb | awk -F" " '{print $2,$3}' > /tmp/rrqm.and.wrqm.tmp && mv /tmp/rrqm.and.wrqm.tmp /tmp/rrqm.and.wrqm
#
# this script parses /tmp/rrqm.and.wrqm and grabs out rrqm/s and wrqm/s and alerts etc.
#  $ cat /tmp/rrqm.and.wrqm
#  0.00 0.03
#
# errors are passed to nagios based on the thresholds passed as arguments;
#   usage ./check_disk_queues.sh warning_threshold critical_threshold
#
# the thresholds are used for both read and write queues
#
# TO DO:
# 

use 5.010; 
use strict;
use warnings;
use Scalar::Util 'looks_like_number';

## PREAMBLE AND SETUP ##
#
my $iostat_check_file = "/tmp/rrqm.and.wrqm";

# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_disk_queues.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

## DO THE CHECK AND PARSE THE OUTPUT
#
# cat out the iostat average from the check file
#  then split the output of that into useful chunks. 
my $results=`cat $iostat_check_file`; chomp($results);
my @results_split = split (/\s+/,$results);  
my $rrqm = $results_split[0]; chomp($rrqm);  
my $wrqm = $results_split[1]; chomp($wrqm);  

# we need to check if the output of the check_mysql command yeilded useful output, or errored.
#  we expect $number_of_threads to be an integer, so lets just check that's the case, else exit out.
if (not looks_like_number $rrqm) { print "ERROR: $results\n";  exit 2; }
if (not looks_like_number $wrqm) { print "ERROR: $results\n";  exit 2; }

#print "rrqm = $rrqm\n";
#print "wrqm = $wrqm\n";

# then drive the logic, returning the specific error codes nagios needs.
# note the performance data after the | delimiter in the print statements.  this is used for graphing, see below.
given($rrqm) {
	when ($rrqm <= $warning_threshold) {
		print "OK: rrqm is $rrqm, wrqm is $wrqm | rrqm=$rrqm;;$warning_threshold;$critical_threshold;0; wrqm=$wrqm;;$warning_threshold;$critical_threshold;0;"; 
		exit 0;
		}
	when ($rrqm > $warning_threshold and $rrqm < $critical_threshold) {
		print "WARNING: rrqm is $rrqm, wrqm is $wrqm | rrqm=$rrqm;;$warning_threshold;$critical_threshold;0; wrqm=$wrqm;;$warning_threshold;$critical_threshold;0;"; 
		exit 1; 
		}
	default {
		print "CRITICAL: rrqm is $rrqm, wrqm is $wrqm | rrqm=$rrqm;;$warning_threshold;$critical_threshold;0; wrqm=$wrqm;;$warning_threshold;$critical_threshold;0;"; 
		exit 2;
		}
}

given($wrqm) {
	when ($wrqm <= $warning_threshold) {
		print "OK: wrqm is $wrqm, rrqm is $rrqm | rrqm=$rrqm;;$warning_threshold;$critical_threshold;0; wrqm=$wrqm;;$warning_threshold;$critical_threshold;0;"; 
		exit 0;
		}
	when ($wrqm > $warning_threshold and $rrqm < $critical_threshold) {
		print "WARNING: wrqm is $wrqm, rrqm is $rrqm | rrqm=$rrqm;;$warning_threshold;$critical_threshold;0; wrqm=$wrqm;;$warning_threshold;$critical_threshold;0;"; 
		exit 1; 
		}
	default {
		print "CRITICAL: wrqm is $wrqm, rrqm is $rrqm | rrqm=$rrqm;;$warning_threshold;$critical_threshold;0; wrqm=$wrqm;;$warning_threshold;$critical_threshold;0;"; 
		exit 2;
		}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value

