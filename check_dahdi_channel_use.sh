#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# parses the output of /usr/sbin/asterisk -rx 'core show channels' and returns the 
#   number of current active ISDN calls 
# returns valid nagios return codes and performance data for pnp4nagios
#
# twsadmin@plover:~$ sudo /usr/sbin/asterisk -rx 'core show channels' | grep -m1 "call" | cut -d' ' -f1
# 0
#
# TO DO:
# 31 is a hardcoded upper bound for the total number of isdn channels.  that's a bit naughty; i should probably do a lookup on that with dahdi_scan;

# NOTE: because this check must run on my old servers, I'm using switch over given/when.
#use 5.010; 
use Switch;
use strict;
use warnings;

## PREAMBLE AND SETUP ##
#
my $maximum_active_channels=31;

# ensure this is being run as root
my $whoami = getpwuid($>);
if ($whoami ne "root") {
	print "This command needs to be run as root (use sudo)\n";
	exit;
}

# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage check_dahdi_channel_use.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

## DO THE CHECK AND PARSE THE OUTPUT
#
# grab the stats 
my $return=`/usr/sbin/asterisk -rx 'core show channels' | grep -m1 "call" | cut -d' ' -f1`; chomp($return);
my @return_split = split (/\s+/,$return);

my $number_of_concurrent_channels=$return_split[0]; 

switch( $number_of_concurrent_channels ) {
	case {$number_of_concurrent_channels < $warning_threshold} {
		print "OK: $number_of_concurrent_channels channels in use | active=$number_of_concurrent_channels;$warning_threshold;$critical_threshold;0;$maximum_active_channels";		
		exit 0;
		}
	case {$number_of_concurrent_channels > $warning_threshold and $number_of_concurrent_channels <= $critical_threshold} {
		print "WARNING: $number_of_concurrent_channels channels in use | active=$number_of_concurrent_channels;$warning_threshold;$critical_threshold;0;$maximum_active_channels";
		exit 1; 
		}
	case {$number_of_concurrent_channels > $critical_threshold}{
		print "CRITICAL: $number_of_concurrent_channels channels in use | active=$number_of_concurrent_channels;$warning_threshold;$critical_threshold;0;$maximum_active_channels";
		exit 2;
		}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value
