#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# parses XXXXXXXXXXXXXX
#
# errors are passed to nagios based on the thresholds passed as arguments;   
#
# TO DO:
# 

use 5.010; 
use strict;
use warnings;
use Scalar::Util 'looks_like_number';
use HTML::Strip;
use POSIX qw(ceil);
use Math::Round;
#use Term::ANSIColor;
#use Term::ANSIColor qw(:constants);
use Getopt::Long;
#use File::Spec;

## PREAMBLE AND SETUP ##
#
# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_adsl_sync.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

my $maximum_concievable_downstream_kilobits_per_second = 8000;
my $maximum_concievable_upstream_kilobits_per_second = 4000;

# grab the password from elsewhere .. 
my $passwordfile = "/etc/adslmodempassword";
my $adsl_modem_password = `cat $passwordfile`; chomp($adsl_modem_password);

## DO THE CHECK AND PARSE THE OUTPUT
#
# grab the stats from the modem as a large string
my $return=`curl --silent --user admin:$adsl_modem_password http://router/configuration/adsl.html?ImPorts.a1 | egrep -A 1 'Upstream|Downstream|Elaspsed|Annex Type'`;chomp($return);
# strip out the html
my $hs = HTML::Strip->new(); my $clean_text = $hs->parse( $return );	
# put all that into a nice array.
my @clean_text_split = split (/\s+/,$clean_text);

# we need to check if the output of the XXXX command yeilded useful output, or errored.
# ??????????
	
# PARSE DATA OUT OF THE $clean_text_split ARRAY
my $connection_uptime_string= join(" ",(@clean_text_split[12..19]));
my ($day,$hr,$min,$sec) = ($connection_uptime_string =~ m/(\d+)/gi); 
#
# uptime
my $connection_uptime_in_seconds = (($day*3600*24) + ($hr * 3600) + ($min * 60) + $sec); 
my $connection_uptime_in_minutes = ceil($connection_uptime_in_seconds/60);
# adsl annex type
my $adsl_annex_type=$clean_text_split[2]; 
#
# connection speed - bits
my $down_bits_per_second=$clean_text_split[8]; 
my $up_bits_per_second=$clean_text_split[5];
my $down_kilobits_per_second=ceil($down_bits_per_second/1000); 
my $up_kilobits_per_second=ceil($up_bits_per_second/1000);
#my $down_megabits_per_second=nearest(.5,$down_bits_per_second/1000/1000); 
#my $up_megabits_per_second=nearest(.5,$up_bits_per_second/1000/1000);
#
# connection speed - bytes
#my $down_kilobytes_per_second=ceil($down_bits_per_second/8/1024); 
#my $up_kilobytes_per_second=ceil($up_bits_per_second/8/1024);
#my $down_megabytes_per_second=nearest(.05,$down_kilobytes_per_second/1024); 
#my $up_megabytes_per_second=nearest(.05,$up_kilobytes_per_second/1024);

# then drive the logic, returning the specific error codes nagios needs.
# note the performance data after the | delimiter in the print statements.  this is used for graphing, see below.

given($down_kilobits_per_second) {
	when ($critical_threshold > $down_kilobits_per_second) {
		print "CRITICAL: Synced at $down_kilobits_per_second kbit/s | down=$down_kilobits_per_second"."kbit/s;$warning_threshold;$critical_threshold;0;$maximum_concievable_downstream_kilobits_per_second";
		exit 2;
		}
	when ($warning_threshold > $down_kilobits_per_second) {
		print "WARNING: Synced at $down_kilobits_per_second kbit/s | down=$down_kilobits_per_second"."kbit/s;$warning_threshold;$critical_threshold;0;$maximum_concievable_downstream_kilobits_per_second";
		exit 1;
		}		
	default {
		print "OK: Synced at $down_kilobits_per_second kbit/s | down=$down_kilobits_per_second"."kbit/s;$warning_threshold;$critical_threshold;0;$maximum_concievable_downstream_kilobits_per_second"; 		
		exit 0;
		}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value

