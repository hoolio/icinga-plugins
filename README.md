icinga-plugins
==============
Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt


These are NRPE plugins for the monitoring system icinga/nagios.  Usage assumes you know how to add
NRPE plugins 

They fall under various categories, and fill the gap between other plugins i couldn't find elsewhere.
Some accept arguments, some dont; they will complain if not invoked	correctly.
All checks return valid performance data for use with the pnp4nagios graphing plugin.

Internet; 
	check_adsl_sync.sh - talks to my adsl router and returns the adsl sync speed
	check_adsl_uptime.sh - talks to my adsl router and returns the uptime in seconds.

MythTV
	check_dvbencoder_use.sh - talks to my myth instance on localhost and returns the number of active encoders
	check_shepherd_epg.sh - talks to my myth instance on localhost and returns the remaining days worth of EPG data

Mysql
	check_mysql_threads.sh - talks to the mysql instance on localhost and returns the number of active threads
	
