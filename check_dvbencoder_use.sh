#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# parses the output of mythtv-status and returns how many encoders (tv tuners) are in use.
# will accept arguments for warning and critical levels .. etc.
#
# none in use looks like this;
#hoolio@ferntree:~/scripts/icinga-plugins$ /usr/bin/lynx -dump localhost:6544/Status/GetStatusHTML | grep HDHOMERUN
#   Encoder 1 [ HDHOMERUN : 12111D6A-0 ] is local on ferntree and is
#   Encoder 2 [ HDHOMERUN : 12111D6A-0 ] is local on ferntree and is not
#   Encoder 3 [ HDHOMERUN : 12111D6A-1 ] is local on ferntree and is
#   Encoder 4 [ HDHOMERUN : 12111D6A-1 ] is local on ferntree and is not
#

use 5.010; 
use strict;
use warnings;

## PREAMBLE AND SETUP ##
#

## DO THE CHECK AND PARSE THE OUTPUT
#
# grab the stats from the modem as a large string
my $return=`/usr/bin/lynx -dump localhost:6544/Status/GetStatusHTML | grep HDHOMERUN`;

# do some regex counting foo 
my $x = "and is";
my $total_number_of_encoders = () = $return =~ /$x/g;

$x = "and is not";
my $total_number_of_idle_encoders = () = $return =~ /$x/g;

my $total_number_of_busy_encoders = $total_number_of_encoders - $total_number_of_idle_encoders;

# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_dvbencoder_use.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

# we need to check if the output of the XXXX command yeilded useful output, or errored.
# ??????????
	
# return stuff for nagios
given($total_number_of_busy_encoders) {
	when ($total_number_of_busy_encoders >= $critical_threshold) {
		print "CRITICAL: $total_number_of_busy_encoders encoder(s) of $total_number_of_encoders busy. | busy=$total_number_of_busy_encoders;;;0;$total_number_of_encoders";
		exit 2;
		}
	when ($total_number_of_busy_encoders < $critical_threshold and $total_number_of_busy_encoders >= $warning_threshold) {
		print "WARNING: $total_number_of_busy_encoders encoder(s) of $total_number_of_encoders busy. | busy=$total_number_of_busy_encoders;;;0;$total_number_of_encoders";
		exit 1;
		}		
	default {
		print "OK: $total_number_of_busy_encoders encoder(s) of $total_number_of_encoders busy. | busy=$total_number_of_busy_encoders;;;0;$total_number_of_encoders";		
		exit 0;
		}
}

# This is how the performance data is formatted;
#  rta=1;2;3;4;5
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value

