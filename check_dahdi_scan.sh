#!/usr/bin/perl -w
#
# Copyright (C) 2010 Local Matters, Inc.
# http://www.localmatters.com/
# Author: Joel C. Maslak
#
# Licensed under GPL version 3
#

use strict;

use Carp;

my %ignore;

MAIN: {
        my @out = `/usr/sbin/dahdi_scan`;

        for my $ig (@ARGV) {
                $ignore{$ig} = 1;
        }

        my $alarm;
        my $desc;
        my @alarms;

        for my $line (@out) {
                chomp($line);

                if ($line =~ /^alarms=/) {
                        $alarm = $line;
                        $alarm =~ s/^alarms=//;
                }
                if ($line =~ /^description=/) {
                        $desc = $line;
                        $desc =~ s/^description=//;
                        if (!defined($ignore{$desc})) {
                                if ($alarm ne 'OK') {
                                        push @alarms, "$desc: $alarm Alarm";
                                }
                        }
                }
        }

        if (scalar(@alarms) > 0) {
                my $out = join '; ', @alarms;
                print "Circuits in alarm: $out\n";
                exit(2);
        } else {
                print "All monitored circuits OK\n";
                exit(0);
        }

}
