#!/usr/bin/perl -w
#
#    Copyright 2008-2014 Julius Roberts.
#
# SIMPLE CHECK TO RETURN ERROR IF DRUSH SAYS DRUPAL NEEDS SECURITY PATCH
#  modified to provide performance data for pnp4nagios, see comments below
#
# NOTES: Implimented workaround for drush null return issue.

# use some perl modules
use strict;
use String::Random;

# generate a random filename for temporary data storage
my $foo = new String::Random;
my $random_string = $foo->randpattern("CnCnCnCnCnCnCnCnCn");
my $random_file = "/tmp/$random_string";

# later we will store the previous return value to smooth aberations with drush.
my $previous_return_value_file = "/tmp/icinga_drupal_patches_previous_return_value";

# setup the other variables
my $argument0 = $ARGV[0];
my $drupal_root = $argument0;
my $pending_drupal_security_updates = 0;
my $up_to_date_modules = 0;
my $whatever_the_previous_return_value_was = `cat $previous_return_value_file`; 

if (not defined $drupal_root){
	print "ERROR: No drupal root defined, eg /var/www/www.wilderness.org.au\n";	
	exit 2;	
}
	
# ask drush what is going on
`/usr/bin/drush -r $drupal_root pm-update -y -s /dev/null 2>&1 > $random_file`;

# check to see how many modules are "up to date".  we will use this later to determine if drush is behaving normally.
$up_to_date_modules = `grep "Up to date" $random_file | wc -l`; chomp($up_to_date_modules);

# count the number of pending drupal security updates
$pending_drupal_security_updates= `grep "SECURITY UPDATE available" $random_file | wc -l`; chomp($pending_drupal_security_updates);

# remove the temporary file because we don't need it anymore.
`rm $random_file`;

# if the number of up_to_date_modules < 1 then we are assuming that there is something wrong with drush.
#  we know that happens quite often, so in order to stop flappage, we will exit using whatever_the_previous_return_value_was.
if ($up_to_date_modules < 1) { 	
	print "CRITICAL: Drush returned no results!!\n";	
	exit $whatever_the_previous_return_value_was;	
} else {
	if ($pending_drupal_security_updates <= 0) {
		print "OK: No security updates pending. | patches=$pending_drupal_security_updates;;;0;";
		`echo 0 > $previous_return_value_file`;
		exit 0;
	} else {
		print "CRITICAL: $pending_drupal_security_updates security updates pending. | patches=$pending_drupal_security_updates;;;0;";
		`echo 2 > $previous_return_value_file`;
		exit 2;
	}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value
