#!/usr/bin/perl -w
#
# Copyright 2014 Julius Roberts.  Released under the GPLv3 http://www.gnu.org/licenses/gpl.txt
#
# parses the output of Mark Suter's internode-quota-check script and wraps it up nicely for nrpe
#
# $ /usr/local/scripts/internets/internode.quota.check.sh
#   username: 134.336 GB (89.6%) and 16.4 days (58.7%) left on 150 GB, 24 Mb/s plan.
#
# $ ./check_internode_quota.sh 140 130
#   WARNING: 134.336 GB of 150 remaining | remaining=134.336gb;140;130;0;150

use 5.010; 
use strict;
use warnings;
use Scalar::Util 'looks_like_number';

## PREAMBLE AND SETUP ##
#
# parse arguments, make sure we have enough
if (($#ARGV + 1) < 2) { print "ERROR; usage ./check_internode_quota.sh warning_threshold critical_threshold\n"; exit 2;}
my $warning_threshold = "$ARGV[0]\n"; chomp($warning_threshold); 
my $critical_threshold = "$ARGV[1]\n"; chomp($critical_threshold);

## DO THE CHECK AND PARSE THE OUTPUT
#
# we need to set the fetchmail environment variable so that the credentials get passed through OK
#my $fetchmail_path="/var/lib/nagios/.fetchmailrc";
$ENV{'FETCHMAILHOME'} = "/var/lib/nagios/.fetchmailrc";

# grab the stats from the modem as a large string
my $return=`/usr/local/scripts/internets/internode.quota.check.sh`; chomp($return);
my @return_split = split (/\s+/,$return);

my $total_data_gb=$return_split[10]; 
my $remaining_data_gb=$return_split[1]; 

my $remaining_days_until_quota_reset=$return_split[5]; 
my $remaining_days_until_quota_reset_percentage=$return_split[7]; $remaining_days_until_quota_reset_percentage =~ s/[()%]//g;
my $total_days_between_quota_reset=30.4; # 365 / 12.  ie monthly.

# we need to check if the output of the quota check yeilded useful output, or errored.
#  we expect $number_of_threads to be an integer, so lets just check that's the case, else exit out.
if (not looks_like_number $total_data_gb) { print "ERROR: $return\n";  exit 2; }

given($remaining_data_gb) {
	when ($remaining_data_gb >= $warning_threshold) {
		print "OK: $remaining_data_gb GB of $total_data_gb remaining | 	data_left=$remaining_data_gb"."gb;$warning_threshold;$critical_threshold;0;$total_data_gb | days_left=$remaining_days_until_quota_reset"."days;;;0;$total_days_between_quota_reset";
		exit 0;
		}
	when ($remaining_data_gb >= $critical_threshold) {
		print "WARNING: $remaining_data_gb GB of $total_data_gb remaining | data_left=$remaining_data_gb"."gb;$warning_threshold;$critical_threshold;0;$total_data_gb | days_left=$remaining_days_until_quota_reset"."days;;;0;$total_days_between_quota_reset";
		exit 1; 
		}
	default {
		print "CRITICAL: $remaining_data_gb GB of $total_data_gb remaining | data_left=$remaining_data_gb"."gb;$warning_threshold;$critical_threshold;0;$total_data_gb | days_left=$remaining_days_until_quota_reset"."days;;;0;$total_days_between_quota_reset";
		exit 2;
		}
}

# This is how the performance data is formatted;
#
#  rta=2.687ms;3000.000;5000.000;0;
#   |    |  |    |         |     | |
#   |----|--|----|---------|-----|-|----- * label 
#        |--|----|---------|-----|-|----- * current value
#           |----|---------|-----|-|----- unit ( UOM = UNIT of Measurement ) 
#                |---------|-----|-|----- warning threshold
#                          |-----|-|----- critical threshold 
#                                |-|----- minimum value 
#                                  |----- maximum value

